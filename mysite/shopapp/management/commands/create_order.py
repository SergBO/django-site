from typing import Sequence

from django.core.management import BaseCommand
from django.contrib.auth import get_user_model
from django.db import transaction

from shopapp.models import Order, Product


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **options):
        # with transaction.atomic():
        #     ...
        self.stdout.write("Create order with products")
        user = get_user_model().objects.get(username="botalov")
        products: Sequence[Product] = Product.objects.all()
        order, created = Order.objects.get_or_create(
            delivery_address="ul Kalinina, d 12/2",
            promocode="PRODUCTS111",
            user=user,
        )
        for product in products:
            order.products.add(product)
        order.save()
        self.stdout.write(f"Created order {order}")
