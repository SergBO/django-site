from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views

app_name = "shopapp"

routers = DefaultRouter()
routers.register("products", views.ProductViewSet)


urlpatterns = [
    path("", views.ShopIndexView.as_view(), name="index"),
    path("api/", include(routers.urls)),
    path("products/", views.ProductsListView.as_view(), name="products_list"),
    path("products/export/", views.ProductsDataExportView.as_view(), name="products-export"),
    path("products/create/", views.ProductCreateView.as_view(), name="product_create"),
    path("products/<int:pk>/", views.ProductDetailView.as_view(), name="product_detail"),
    path("products/<int:pk>/update/", views.ProductUpdateView.as_view(), name="product_update"),
    path("products/<int:pk>/archive/", views.ProductDeleteView.as_view(), name="product_delete"),
    path("orders/", views.OrdersListView.as_view(), name="orders_list"),
    path("orders/<int:pk>/", views.OrderDetailView.as_view(), name="order_details"),
    path("groups/", views.GroupListView.as_view(), name="groups"),

]
