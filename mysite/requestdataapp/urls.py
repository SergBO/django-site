from django.urls import path

from . import views

urlpatterns = [
    path("get/", views.process_get_view, name="get-view"),
    path("bio/", views.user_form, name="bio-form"),
    path("upload/", views.handel_file_upload, name="file-upload"),
]
