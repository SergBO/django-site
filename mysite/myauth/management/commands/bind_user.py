from typing import Any
from django.contrib.auth.models import User, Group, Permission
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args: Any, **options: Any) -> str | None:
        user = User.objects.get(pk=4)
        group, created = Group.objects.get_or_create(
            name="profile_maneger",
        )
        permission_profile = Permission.objects.get(
            codename="view_profile",
        )
        permission_logentry = Permission.objects.get(
            codename="view_logentry",
        )

        # добавление разрешия в группу
        group.permissions.add(permission_profile)

        # присоединение пользователя к группе
        user.groups.add(group)

        # связать пользователя на прямую с разрешением
        user.user_permissions.add(permission_logentry)

        group.save()
        user.save()